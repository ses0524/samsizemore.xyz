---
title: 'Sam Sizemore'
---
{{< figure class=profile src="/images/me.JPG" >}}

## Welcome to my website! 	
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     || 

This is my small space on the web. Enjoy!
## My Links:
[Gitlab](https://gitlab.com/ses0524)
 
[Yesterday Development](https://yesdev.io)

[Email](mailto:samuel.sizemore08@gmail.com)

## Read some of my posts below:
