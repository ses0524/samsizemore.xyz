---
title: "About Me"
date: 2022-09-04T16:50:27-04:00
tags: ['about', 'posts']
---
I decided to start this website as a project to better learn the concepts of the web development and to have a small slice of the world wide web.

I am currently a solo sysadmin at a small company and am working on learning coding and cybersecurity. 

In my free time: I take pitures, play games, and occationally work on projects.
