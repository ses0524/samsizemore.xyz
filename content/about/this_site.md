---
title: "This Site"
date: 2022-01-01
tags: ['about', 'posts']
---
This site is powered by [Hugo](https://gohugo.io/) with the [Lugo](https://github.com/LukeSmithxyz/lugo) theme, and [Nginx](https://www.nginx.com/)

Special thanks to [Matt Yates](https://yesdev.io) and [Luke Smith](https://lukesmith.xyz) for guides and and how to's. 
